
/*A natural number with n digits is called an Armstrong number if the sum of its digits raised to the power of n is equal to the number itself.
Find all Armstrong numbers from 1 to k. To solve the problem, use the division of code into methods */


public class armstrongNumbers {

    public static void findingArmstrongNumbers(int k) {
        for (int i = 1; i <= k; i++) {
           if(isArmstrong(i)) {
               System.out.println(i + "- is an Armstrong number");
           }
        }
    }

    public static boolean isArmstrong (int number) {

        int originalNumber = number;
        int length = lengthOfTheNumber(number);
        int sum = 0;
        for (int i = 0; i < length; i++) {

            int digit = number % 10;

            sum += Math.pow(digit, length);
            number = number / 10;
        }
        return sum == originalNumber;
    }

    public static int lengthOfTheNumber (int number) {

        int length = 0;
        while (number > 0) {
            number /= 10;
            length++;
        }

        return length;
    }
}
