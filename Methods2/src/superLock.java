import java.util.Scanner;

public class superLock {

    public static void fillingTheCells(int firstNumber, int positionOfTheFirst, int secondNumber, int positionOfTheSecond) {
        for (int i = 1; i < 7; i++) {
            int [] cells = new int[10];
            cells[positionOfTheFirst] = firstNumber;
            cells[positionOfTheSecond] = secondNumber;
            if(positionOfTheFirst < 1 && positionOfTheFirst + 1 != positionOfTheSecond) {
                cells[positionOfTheFirst + 1] = i;
            } else if (positionOfTheFirst >= 1 && positionOfTheFirst - 1 != positionOfTheSecond) {
                cells[positionOfTheFirst - 1] = i;
            }
            fillingToTheLeft(cells, positionOfTheFirst, positionOfTheSecond);
            fillingToTheRight(cells, positionOfTheFirst, positionOfTheSecond);
            if (checkCells(cells)) {
                output(cells);
                return;
            }
        }

        System.out.println("Not able to unlock this");
    }

    public static void fillingToTheLeft (int [] cells, int positionOfTheFirst, int positionOfTheSecond ){

        for (int i = positionOfTheFirst - 2; i >= 0; i--) {

            if (cells[i] != 0 ){
                continue;
            }

            int previousIndex = i + 1;
            int beforePreviousIndex = i + 2;

            if (i == positionOfTheSecond){
                if(cells[i] + cells[previousIndex] + cells[beforePreviousIndex] != 10){
                    return;
                }
                continue;
            }

            cells[i] = 10 - cells[previousIndex] - cells[beforePreviousIndex];

            if (cells[i] > 6 || cells[i] < 1 ){
                return;
            }
        }

    }
    public static void fillingToTheRight (int [] cells, int positionOfTheFirst, int positionOfTheSecond ){
        for (int i = positionOfTheFirst + 1; i < 10; i++){

            if (cells[i] != 0 ){
                continue;
            }
            int previousIndex = i - 1;
            int beforePreviousIndex = i - 2;

            if (i == positionOfTheSecond){
                if(cells[i] + cells[previousIndex] + cells[beforePreviousIndex] != 10){
                    return;
                }
                continue;
            }


            cells[i] = 10 - cells[previousIndex] - cells[beforePreviousIndex];

            if (cells[i] > 6 || cells[i] < 1 ){
                return;
            }
        }
    }

    public static void output (int[] cells){

        for (int cell : cells) {
            System.out.print(cell + " ");
        }
    }

    public static int input (String text) {
        System.out.println(text);
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        return number;
    }
    public static boolean checkCells (int [] cells){

        for (int i = 2; i < cells.length; i++){

            if (cells[i] + cells[i-1] + cells[i-2] != 10){
                return false;
            }

        }
        return true;
    }

}