public class sumsOfArrayElements {

    //4. An array D is given. Determine the following sums: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6].

    public static int sumOfConsecutive (int firstIndex, int lastIndex) {

        int sum = 0;
        int [] D = new int[]{11,12,13,14,15,16};

        for (int i = firstIndex; i <= lastIndex;i++){

            sum += D[i];
        }
        return sum;
    }
}
