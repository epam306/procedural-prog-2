import java.util.Scanner;

public class additionOfUnlimited {


    public static String unlimitedAddition(String typedNumber1, String typedNumber2) {

        String number1;
        String number2;
        char minus = ' ';// second typed number by the user

        if (typedNumber1.charAt(0) == '-' && typedNumber2.charAt(0) == '-') {         //checking if 2 numbers are negative
            number1 = typedNumber1.replace("-", "");
            number2 = typedNumber2.replace("-", "");
            minus = '-';
        } else {
            number1 = typedNumber1;
            number2 = typedNumber2;
        }
        String sumOfTwo = "";
        int length = Math.max(number1.length(), number2.length());
        int radix = 0;                                                                  // radix ( the 1 that can appear if the sum of two digits is bigger than 9)


        if (number1.length() > number2.length()) {
            int count = number1.length() - number2.length();
            for (int i = 0; i < count; i++) {
                number2 = '0' + number2;
            }
        } else if (number1.length() < number2.length()) {
            int count = number2.length() - number1.length();
            for (int i = 0; i < count; i++) {
                number1 = '0' + number1;
            }
        }


        for (int i = 1; i <= length; i++) {

            char lastDigit;                          // last digit of the number
            int number = Integer.parseInt(String.valueOf(number1.charAt(number1.length() - i))) + Integer.parseInt(String.valueOf(number2.charAt(number2.length() - i))) + radix;

            if (number > 9) {
                lastDigit = (char) ((number % 10) + '0');
                radix = 1;
            } else {
                lastDigit = (char) (number + '0');
                radix = 0;
            }
            sumOfTwo = lastDigit + sumOfTwo;
        }
        if (radix == 1) {
            sumOfTwo = '1' + sumOfTwo;
        }

        return minus + sumOfTwo;

    }

    public static String unlimitedSubtraction(String firstNumber, String secondNumber) {

        String negativeNumber;
        String positiveNumber;
        int radix = 0;
        String subtractionValue = "";
        String biggestNumber;
        String smallestNumber;
        char minus = '-';

        if (firstNumber.charAt(0) == '-') {
            negativeNumber = firstNumber.replace("-", "");
            positiveNumber = secondNumber;
        } else {
            negativeNumber = secondNumber.replace("-", "");
            positiveNumber = firstNumber;
        }
        if (negativeNumber.length() > positiveNumber.length()) {
            biggestNumber = negativeNumber;
            smallestNumber = positiveNumber;
        } else if (negativeNumber.length() < positiveNumber.length()) {
            biggestNumber = positiveNumber;
            smallestNumber = negativeNumber;
        } else {
            biggestNumber = biggestNumber(negativeNumber, positiveNumber);
            smallestNumber = smallestNumber(negativeNumber, positiveNumber);
            if (biggestNumber == null) {
                return "0";
            }
        }
        int length = Math.max(negativeNumber.length(), positiveNumber.length());
        int minLength = Math.min(negativeNumber.length(), positiveNumber.length());


        for (int i = 1; i <= length; i++) {

            char lastDigit;
            int number;
            if (i <= minLength) {
                number = Integer.parseInt(String.valueOf(biggestNumber.charAt(biggestNumber.length() - i))) - Integer.parseInt(String.valueOf(smallestNumber.charAt(smallestNumber.length() - i))) - radix;
            } else {
                number = Integer.parseInt(String.valueOf(biggestNumber.charAt(biggestNumber.length() - i))) - radix;
            }
            if (number < 0) {
                number = 10 + number;
                radix = 1;
                lastDigit = (char) (number + '0');
            } else {
                lastDigit = (char) (number + '0');
                radix = 0;
            }

            subtractionValue = lastDigit + subtractionValue;
        }

        int i = 0;
        String finalString = subtractionValue;
        if ((subtractionValue.charAt(i)) == '0') {
            while ((subtractionValue.charAt(i)) == '0') {
                finalString = subtractionValue.substring(i + 1);
                i++;
            }
        }
        if (biggestNumber.equals(negativeNumber)) {
            finalString = minus + finalString;
        }
        return finalString;
    }

    public static String biggestNumber(String negativeNumber, String positiveNumber) {

        String biggest;

        for (int i = 0; i < negativeNumber.length(); i++) {
            int negative = Integer.parseInt(String.valueOf(negativeNumber.charAt(i)));
            int positive = Integer.parseInt(String.valueOf(positiveNumber.charAt(i)));
            if (negative - positive < 0) {
                biggest = positiveNumber;
                return biggest;
            } else if (negative - positive > 0) {
                biggest = negativeNumber;
                return biggest;
            }
        }

        return null;
    }

    public static String smallestNumber(String negativeNumber, String positiveNumber) {

        String smallest;

        for (int i = 0; i < negativeNumber.length(); i++) {
            int negative = Integer.parseInt(String.valueOf(negativeNumber.charAt(i)));
            int positive = Integer.parseInt(String.valueOf(positiveNumber.charAt(i)));
            if (negative - positive > 0) {
                smallest = positiveNumber;
                return smallest;
            } else if (negative - positive < 0) {
                smallest = negativeNumber;
                return smallest;
            }
        }

        return null;
    }

    public static String sum(String number1, String number2) {

        boolean isNumber1Negative = number1.charAt(0) == '-';
        boolean isNumber2Negative = number2.charAt(0) == '-';

        if ((isNumber1Negative && !isNumber2Negative) || (isNumber2Negative && !isNumber1Negative)) {
            return additionOfUnlimited.unlimitedSubtraction(number1, number2);
        } else {
            return additionOfUnlimited.unlimitedAddition(number1, number2);
        }
    }

}


