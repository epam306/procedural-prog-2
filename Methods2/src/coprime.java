public class coprime {

    public static boolean isCoprime (int a, int b, int c) {

        int smallest = Math.min(Math.min(Math.abs(a),Math.abs(b)),Math.abs(c));

        if (a == 0 || b == 0 || c == 0) {
            return false;
        }
        if (a % smallest == 0 && b % smallest == 0 && c % smallest == 0){
            return false;
        }

        for (int i = (smallest / 2); i > 1; i-- ) {

            if (a % i == 0 && b % i == 0 && c % i == 0) {

                return false;
            }
        }

        return true;
    }
}
